$(function () {
    var offset = 0;
    var skip = 10;
    var posts = [];

    // Get all of the blog posts and stores them
    (function () {
        $.ajax({
            url: "https://api.parse.com/1/classes/BlogPost",
            type: "GET",
            dataType: 'json',
            data: {
                'order': '-publish_date'
            },
            headers: {
                "X-Parse-Application-Id": "DqASTLWjUVX991OcCse60944QtGLLs5J0Q9PyU7e",
                "X-Parse-REST-API-Key": "Y9sfBXIa3CeTzPZuGKAki5BOVSLbuwCLWRkuwnwo",
                "Content-Type": "application/json"
            },
            success: function (data) {
                posts = data.results;

                var output = '';
                var length = Math.min(skip, posts.length);

                for (offset; offset < length; offset++) {
                    var test = '<div class = "blog-format" >';
                    test += '<div class="row">';
                    test += '<div  class="col-md-8 blog-title">' + posts[offset].title + '</div>';
                    test += '<div class="col-md-4 blog-time">' + formatDate(posts[offset].publish_date) + '</div>';
                    test += '</div>';
                    test += '<p class="blog-description">' + posts[offset].content + '</p>';
                    test += '</div>';
                    output += test;
                }
                $('#result').html(output);
            },
            error: function (error) {
                $('#blog-error').removeClass("hidden");
            }
        });
        return false;
    })();

    // Add more blog posts when we reach the end of the page
    $(window).scroll(function () {

        if ($(this).scrollTop() >= $('#result').height() - $(this).height()) {

            var length = Math.min(offset + skip, posts.length);

            for (offset; offset < length; offset++) {
                console.log(offset + ": " + JSON.stringify(posts[offset]));
                addPost(posts[offset]);
            }
        }
    });

    // Create a new post by cloning an existing one and changing the data
    function addPost(post) {
        var next_post = $("#result > div:last").clone();
        next_post.find(".blog-title").html(post.title);
        next_post.find(".blog-time").html(formatDate(post.createdAt));
        next_post.find(".blog-description").html(post.description);
        next_post.appendTo("#result");
    }

    // Formates the date
    function formatDate(date) {
        return moment(date.toString()).format('DD MMMM YYYY HH:mm');
    }

});
