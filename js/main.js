$(function () {

    // Display certain icons/text once the page has loaded
    $('.animate-onload').each(function () {
        animate($(this), parseInt($(this).data("onload")));
    });


    // Display Icons on scroll
    $(window).scroll(function () {
        $('.animate, .animate-hidden').each(function () {
            var imagePos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();

            // Is the element in the viewport
            if (imagePos < topOfWindow + $(window).height()) {
                animate($(this));
            }
        });
    });


    // Footer Icons Hover effect
    $('#twitter-icon, #facebook-icon, #linkedin-icon, #googleplus-icon').hover(function () {
        $(this).toggleClass('pulse');
    });

    // Increment figures from 0 to final value after animation end
    $('.animate-figure').one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function (e) {
        var me = $(this);
        var start = 0;
        var end = parseInt($(this).data('count')) || 0;
        var increment = Math.ceil(end / 40);

        var counter = setInterval(function (el) {

            start += increment;
            me.text(start);

            if (start >= end) {
                me.text(end);
                clearInterval(counter);
            }
        }, 50);

    });

    // Animate header carousel text when a slide is in view
    $('#header-carousel').on('slid.bs.carousel', function () {
        $('.carousel-text > animate, .carousel-text > .animate-hidden').each(function () {
            animate($(this));
        });

    });

    // Reset all animations when sliding to another slide
    $('#header-carousel').on('slide.bs.carousel', function () {
        $('.carousel-text > animate, .carousel-text > .animate-hidden').each(function () {
            var classes = $(this).data("animations");
            $(this).stop(true, true).removeClass(classes);
        });
    });


    /**
     * Applies the animation to the specified object
     * @param {JQuery} el - The element being animated
     */
    function animate(el, delay_time) {
        var classes = el.data("animations");
        var delay = delay_time || parseInt(el.data("delay"));

        // Add classes after a delay if one exists
        if (delay) {

            el.delay(delay).queue(function () {
                el.addClass(classes).dequeue();
            });
        } else {
            el.addClass(classes);
        }
    }

});


/*projects page  ---- showcase of technology */

var Tabs = {

  init: function() {
    this.bindUIfunctions();
    this.pageLoadCorrectTab();
  },

  bindUIfunctions: function() {

    // Delegation
    $(document)
      .on("click", ".transformer-tabs a[href^='#']:not('.active')", function(event) {
        Tabs.changeTab(this.hash);
        event.preventDefault();
      })
      .on("click", ".transformer-tabs a.active", function(event) {
        Tabs.toggleMobileMenu(event, this);
        event.preventDefault();
      });

  },

  changeTab: function(hash) {

    var anchor = $("[href=" + hash + "]");
    var div = $(hash);

    // activate correct anchor (visually)
    anchor.addClass("active").parent().siblings().find("a").removeClass("active");

    // activate correct div (visually)
    div.addClass("active").siblings().removeClass("active");

    // update URL, no history addition
    
    // window.history.replaceState("", "", hash);

    // Close menu, in case mobile
    anchor.closest("ul").removeClass("open");

  },

  // If the page has a hash on load, go to that tab
  pageLoadCorrectTab: function() {
    this.changeTab(document.location.hash);
  },

  toggleMobileMenu: function(event, el) {
    $(el).closest("ul").toggleClass("open");
  }

}

Tabs.init()

