function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: {
            lat: 51.494335,
            lng: -0.061212
        },
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [
                google.maps.MapTypeId.ROADMAP,
                google.maps.MapTypeId.SATELLITE
            ],
            position: google.maps.ControlPosition.BOTTOM_LEFT
        }
    });

    var mapInfo = document.getElementById('map-info');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapInfo);

    // Append a Save Control to the existing map-info div.
    var saveWidget = new google.maps.SaveWidget(mapInfo, {
        place: {
            // ChIJ208KoD0DdkgR6inCYq5NeXU is the place Id for the biscuit factory
            placeId: 'ChIJ208KoD0DdkgR6inCYq5NeXU',
            location: {
                lat: 51.494335,
                lng: -0.061212
            }
        },
        attribution: {
            source: 'Google Maps JavaScript API',
            webUrl: 'https://developers.google.com/maps/'
        }
    });

    var marker = new google.maps.Marker({
        map: map,
        position: saveWidget.getPlace().location
    });
}
